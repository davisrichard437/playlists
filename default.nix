dm: # declarative-music functions

let

  importDirectory =
    dir:
    let
      files = builtins.readDir dir;
    in
    builtins.mapAttrs (name: _: import (dir + "/${name}") dm) files;

in
importDirectory ./playlists
