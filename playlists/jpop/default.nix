dm:

let

  inherit (dm) mkAttrPlaylist fetchYTM fetchYTTM;

in

mkAttrPlaylist {
  name = "JPop";

  songs = {
    "Ado" = {
      Backlight = fetchYTM "gt-v_YCkaMY";
      "Gira Gira" = fetchYTM "sOiMD45QGLs";
      Usseewa = fetchYTM "Qp3b-RXtz4w";
    };

    Akeboshi = {
      Wind = fetchYTM "WqGOaOHu5uY";
    };

    Ali = {
      "Wild Side" = fetchYTM "u5heWZ9occg";
    };

    "Ali, AKLO" = {
      "LOST IN PARADISE" = fetchYTM "tLsJQ5srVQA";
    };

    "Amane Kanata" = {
      "To Me" = fetchYTM "_qmAo83zOUg";
    };

    "Amane Kanata, Nekomata Okayu" = {
      "Call Boy" = fetchYTM "sxMUJSK5u4g";
    };

    "Amane Kanata, Tokoyami Towa" = {
      Madoromi = fetchYTM "23nEnPOXLEk";
    };

    "Arianne" = {
      "Komm Susser Todd" = fetchYTM "hoKluzn07eQ";
    };

    "ASIAN KUNG-FU GENERATION" = {
      "Haruka Kanata" = fetchYTM "nJ6A6GC_ki4";
    };

    "Ayunda Risu" = {
      "ALiCE&u" = fetchYTM "IGviVpVE1fA";
    };

    BRADIO = {
      Fanfare = fetchYTM "kau-MRoDh1c";
      Flyers = fetchYTM "9wh8FgsEtNQ";
    };

    "Burnout Syndromes" = {
      "FLY HIGH!!" = fetchYTM "ljRky5jyOuk";
      hikareare = fetchYTM "9lYppQg7tKc";
    };

    "Calliope Mori" = {
      Bully = fetchYTM "PluDFsVo3z0";
      "Bully (Lofi Ver.)" = fetchYTM "KqM7Fi_tIlI";
      "end of a life" = fetchYTM "BXB26PzV31k";
      Show = fetchYTM "9-J5FmQhugs";
    };

    "Calliope Mori, Gawr Gura" = {
      KING = fetchYTM "qNIhngowViI";
      Q = fetchYTM "aetXqd9B8WE";
    };

    "Calliope Mori, Hoshimachi Suisei" = {
      "GETCHA!" = fetchYTM "EjlMPu5sEgw";
    };

    "Ceres Fauna" = {
      "Cute Girlfriend" = fetchYTM "IdgKmhbsZt0";
      "FAUNA FM" = fetchYTM "omnyBfxavk8";
      "FAUNA SWEEP" = fetchYTM "oGe_-UNTWgY";
      "Let Me Stay Here" = fetchYTM "0RMVJTLZOzQ";
    };

    ClariS = {
      Connect = fetchYTM "ADIP3Um32Gc";
    };

    "Creepy Nuts" = {
      Daten = fetchYTM "HRL5Cp_mPeo";
      "Yofukashi no Uta" = fetchYTM "F6_zbnfxoBA";
    };

    Eve = {
      "Kaikai Kitan" = fetchYTM "1tk1pqwrOys";
    };

    "Fear, and Loathing in Las Vegas" = {
      "Let Me Hear" = fetchYTM "4dOw3TpoHtk";
    };

    FLOW = {
      COLORS = fetchYTM "a_RcfZoBBo8";
      "Niji No Sora" = fetchYTM "Gv0Az2HvEDs";
    };

    "Galileo Galilei" = {
      "Aoi Shiori" = fetchYTM "T3bxbVGWy5k";
    };

    "Gawr Gura" = {
      REFLECT = fetchYTM "WGgEFoI9MhE";
    };

    "Gen Hoshino" = {
      Comedy = fetchYTM "G3qQtf7jahE";
    };

    "Hakos Baelz" = {
      "PLAY DICE!" = fetchYTM "na6bysYNuS0";
      PSYCHO = fetchYTM "xnX8Lk-iSCo";
      STRANGE = fetchYTM "s2wIscd82QE";
    };

    Hemenway = {
      "By My Side" = fetchYTM "3mKRHFe08g0";
    };

    "Home Made Kazoku" = {
      "Shooting Star" = fetchYTM "Q9cT5EAEGyk";
    };

    "Hoshimachi Suisei" = {
      GHOST = fetchYTM "IKKar5SS29E";
      KING = fetchYTM "mLwtfg57kbs";
      "Stellar Stellar" = fetchYTM "a51VH9BYzZA";
    };

    "Houshou Marine" = {
      "Ahoy!!" = fetchYTM "gZL4-f3jRoE";
      "Bloody Stream" = fetchYTM "JYtt0inzDR4";
      "COLORS" = fetchYTM "V_5mwG9M5Eo";
      "I'm Your Treasure Box" = fetchYTM "vV-5W7SFHDc";
      Unison = fetchYTM "_VIeV_LZXHM";
      "Unison (Midnight ver.)" = fetchYTM "eNKb9P1Xp4Q";
      "Yona Yona Yona" = fetchYTM "vFunN0xiZAQ";
    };

    Ikimonogagari = {
      "Blue Bird" = fetchYTM "KpsJWFuVTdI";
    };

    "Ikou Kanako" = {
      Fatima = fetchYTM "hgFPAJNuVhM";
      "Hacking to the Gate" = fetchYTM "ZGM90Bo3zH0";
    };

    KANA-BOON = {
      Silhouette = fetchYTM "dlFA0Zq1k2A";
    };

    "Kenshi Yonezu" = {
      "Kick Back" = fetchYTM "M2cckDmNLMI";
      Lemon = fetchYTM "SX_ViT4Ra7k";
      "Peace Sign" = fetchYTM "9aJVr5tTTWk";
      "Uma to Shika" = fetchYTM "ptnYBctoexk";
    };

    "Konomi Suzuki" = {
      Realize = fetchYTM "uaRnwnmqrws";
      Redo = fetchYTM "R9i8nVS2NCA";
    };

    "Mai Yamane" = {
      Blue = fetchYTM "Xjzb_-26m2o";
      "The Real Folk Blues" = fetchYTM "CjUVTEExfBg";
    };

    "Masayuki Suzuki, Airi Suzuki" = {
      "Daddy ! Daddy ! Do !" = fetchYTM "2Od7QCsyqkE";
    };

    "Masayuki Suzuki, Rikka Ihara" = {
      "Love Dramatic" = fetchYTM "BC4zaaiXNMI";
    };

    "Masayuki Suzuki, Su" = {
      "GIRI GIRI" = fetchYTM "vptdHEUZN10";
    };

    Minami = {
      "Crying for Rain" = fetchYTM "0YF8vecQWYs";
      Hollowness = fetchYTM "HIRiduzNLzQ";
      Lilac = fetchYTM "GQ3V50XoLOM";
      "main actor" = fetchYTM "jb4ybTQwcdw";
      "Waiting for Rain" = fetchYTM "766qmHTc2ro";
    };

    "MONOGATARI Series" = {
      "Ambivalent World" = fetchYTM "esSANfMxtlA";
      "Dark Cherry Mystery" = fetchYTM "_XdwYtDT910";
      "Decent Black" = fetchYTM "A0Iw47S3z90";
      "etoile et toi" = fetchYTM "rzXEw5VTtoo";
      "etoile et toi (edition le blanc)" = fetchYTM "G2hWYfflWB0";
      "Happy Bite" = fetchYTM "461ON5K7AQI";
      Kaerimichi = fetchYTM "Lr-_2PJk9XI";
      "the last day of my adolescence" = fetchYTM "360c_SkOByI";
      Mathemagics = fetchYTM "F_Cr9vZ08Ws";
      "Mein Schatz" = fetchYTM "FJ58Dk_cJKA";
      "Mousou Express" = fetchYTM "y2XArpEcygc";
      "Orange Mint" = fetchYTM "5QdthXZCgEA";
      "Platinum Disco" = fetchYTM "14FOPsSCIPs";
      "Renai Circulation" = fetchYTM "F8xpmTmdf6o";
    };

    "MYTH & ROID" = {
      "Paradisus Paradoxum" = fetchYTM "K5bm_ER9CKw";
      "Styx Helix" = fetchYTM "7uJxtwBFxxI";
      VORACITY = fetchYTM "XPOIe6WdhPQ";
    };

    "Nanashi Mumei" = {
      mumei = fetchYTM "oA0CpI0vCK4";
    };

    "Natsumi Kiyoura" = {
      "Tabi no Tochuu" = fetchYTM "9ydC2pUQpNQ";
    };

    "Nekomata Okayu" = {
      "Doku no Oujisama" = fetchYTM "M8UmWCeIKyQ";
      Flos = fetchYTM "4muYzftomAE";
      "Nemuru Machi" = fetchYTM "WCTiDGl8Di0";
    };

    "Nekomata Okayu, Ookami Mio" = {
      "Otona Rule" = fetchYTM "ESM-OniA96w";
    };

    "Nerissa Ravencroft" = {
      "Down By The River" = fetchYTTM "8j3wK1DXhrs" 0 140;
      "Love Me, Love Me, Love Me" = fetchYTM "RWU3o_kDixc";
    };

    "Ninomae Ina'nis" = {
      "Tabi no Tochuu" = fetchYTM "NIv_yYKl9tQ";
      VIOLET = fetchYTM "8ZdLXELdF9Q";
    };

    nomico = {
      "Bad Apple!!" = fetchYTM "9lNZ_Rnr7Jc";
    };

    "Omaru Polka" = {
      "LOL" = fetchYTM "xfrS_3gRdrg";
    };

    "Ookami Mio" = {
      Howling = fetchYTM "YPm9T622OX0";
      Sparkle = fetchYTM "2l_6oIGTrbg";
    };

    "the peggies" = {
      Centimeter = fetchYTM "676JAs6WJJE";
      Kiminosei = fetchYTM "KbT3JsJmd14";
    };

    PORNOGRAFFITTI = {
      Melissa = fetchYTM "YzJcBq0xufI";
    };

    Sayuri = {
      Heikousen = fetchYTM "XzqwvYT_Rmw";
    };

    Seatbelts = {
      "Bad Dog No Biscuits" = fetchYTM "EuAzPR0ACVw";
      Tank = fetchYTM "acQQ60R-zhY";
      "Waltz for Zizi" = fetchYTM "Qbip5oZVL94";
    };

    "Shinsei Kamattechan" = {
      "My War" = fetchYTM "goXKlOozyx8";
    };

    Shun = {
      "Never Change" = fetchYTM "5XFBRikGwqw";
    };

    SiM = {
      "The Rumbling" = fetchYTM "OBqw818mQ1E";
    };

    "Steve Conte" = {
      Rain = fetchYTM "6GB3_O9-iEw";
    };

    Supercell = {
      "Kimino Shiranai Monogatari" = fetchYTM "8rymXl---MQ";
    };

    "Tokoyami Towa" = {
      ANEMONE = fetchYTM "G3J06ircgMA";
      "-Error" = fetchYTM "3UV8OZj2olg";
      FACT = fetchYTM "Zc05le75CfI";
      Hearts = fetchYTM "wVfp1NPf928";
      Palette = fetchYTM "Ud73fm4Uoq0";
    };

    "Tokoyami Towa, Hoshimachi Suisei" = {
      "Grey and Blue" = fetchYTM "0firv69LkgI";
    };

    "Tsunomaki Watame" = {
      "Everlasting Soul" = fetchYTM "luSusXp0RQU";
    };

    "Ultra Tower" = {
      "Kibou no Uta" = fetchYTM "-AXp0O9PKa0";
    };

    Various = {
      "Fukashigi no Carte" = fetchYTM "KM2_4NpZgUU";
    };

    YOASOBI = {
      Idol = fetchYTM "ZRtdQ81jPUQ";
      Kaibutsu = fetchYTM "dy90tA3TT1c";
      # "Yoru ni Kakeru" = fetchYTM "x8VYWazR5mE";
    };

    "Yoko Takahashi" = {
      "The Cruel Angel's Thesis" = fetchYTM "wqmv96HJan8";
      "FLY ME TO THE MOON" = fetchYTM "J07vEAGtm8U";
    };

    Yoshiki = {
      "Red Swan" = fetchYTM "r1XE8ON8fos";
    };

    YURIKA = {
      "Kyoumen no Nami" = fetchYTM "swi8YKz0fsE";
    };

    "Yuuki Ozaki" = {
      Trigger = fetchYTM "oT33MXomUPs";
    };
  };
}
