dm:

let

  inherit (dm) mkAttrPlaylist fetchYTM fetchYTTM;

in

mkAttrPlaylist {
  name = "Punk Pop";

  songs = {
    "My Chemical Romance" = {
      "The Foundations of Decay" = fetchYTM "V2kWUJkRvVs";

      "Number One" = {
        "Boy Division" = fetchYTTM "93C6_X49ZoI" 0 176;
      };

      "Danger Days: The True Lives of the Fabulous Killjoys" = {
        "The Only Hope for Me Is You" = fetchYTM "fSye1-TBeqg";
        "Scarecrow" = fetchYTM "Yx3X7Tg4axo";
      };

      "The Black Parade" = {
        "This Is How I Disappear" = fetchYTM "paFCInMIPfA";
        "Welcome to the Black Parade" = fetchYTM "G-I9csAflBs";
        "I Don't Love You" = fetchYTM "pqfZYvsTkkk";
        Cancer = fetchYTM "fjpaZQwr30I";
        Mama = fetchYTM "NWDbyjhk_ZA";
        Sleep = fetchYTM "Eij0tbVHgL4";
        Teenagers = fetchYTM "jLKOBJR5vHs";
      };

      "Three Cheers for Sweet Revenge" = {
        "Helena" = fetchYTM "a_1tA0bpDQs";
        "You Know What They Do to Guys Like Us in Prison" =
          fetchYTM "ShQdOf2zAzc";
        "I'm Not Okay (I Promise)" = fetchYTM "UgmY2sB71Hc";
      };

      "I Brought You My Bullets, You Brought Me Your Love" = {
        "Early Sunsets Over Monroeville" = fetchYTM "fOLfblOB2e8";
        "Demolition Lovers" = fetchYTM "GaLaMd_EAo4";
      };
    };

    "Pierce The Veil" = {
      "Collide With The Sky" = {
        "A Match Into Water" = fetchYTM "tjSqm6cdtsM";
        "King For A Day" = fetchYTM "JxyozJA04MA";
        "Tangled In The Great Escape" = fetchYTM "PhLF7_pq1TE";
        "Hell Above" = fetchYTM "RePmnl5ulEg";
        "Bulls In The Bronx" = fetchYTM "09ynXtatJaw";
        "Props & Mayhem" = fetchYTM "7XmjdUkRyTU";
      };
    };

    "Sleeping With Sirens" = {
      "Let's Cheers To This" = {
        "A Trophy Fathers Trophy Son" = fetchYTM "gdT-JG2hb8w";
      };
    };
  };
}
