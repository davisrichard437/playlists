{
  description = "My personal playlists";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    declarative-music = {
      url = "git+https://codeberg.org/quantenzitrone/declarative-music.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";
  };

  nixConfig = {
    auto-optimise-store = true;
    keep-derivations = false;
    keep-outputs = false;
    sandbox = "relaxed";
  };

  outputs = { self, nixpkgs, declarative-music, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        {
          packages = import ./. declarative-music.legacyPackages.${system};
        }
      ) // {
      overlays.default = final: prev:
        {
          playlists = self.packages.x86_64-linux;
        };
    };
}
